// import Protocol from '../udp/Protocol.js'

const SPECIAL_FUNCTIONS = Object.freeze({
  'expoDemo': {
    id: 'expoDemo',
    name: 'Expo Demo',
    idByte: Buffer.from('ED', 'hex'),
    txPeriod: 500
  }
}

)

const SF_ID_POS = 1

export default {
  SPECIAL_FUNCTIONS,
  matchesSFByte (pktContents, idByte) {
    const res = pktContents.compare(idByte, 0, idByte.length, SF_ID_POS, SF_ID_POS + idByte.length)
    return (res === 0)
  },
  getIDByte (sfName) {
    return SPECIAL_FUNCTIONS[sfName].idByte
  }
}
