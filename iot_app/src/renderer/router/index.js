import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: require('@/components/Home').default
    },
    {
      path: '/connection/:ip/:port/:mac/:broadcastPort',
      name: 'Connection',
      component: require('@/components/Connection').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
