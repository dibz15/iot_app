import Server from './Server'
// eslint-disable-next-line
import Peripherals from './Peripherals'

const IOT_HEADER_LEN = 7
const IOT_HEADER = Buffer.from('::IOT::FFFFFFFFFFFF::', 'ascii')
const HEADER_START = 0
const ID_LEN = 12
const PC_HEADER = Buffer.from('::PCG::FFFFFFFFFFFF::', 'ascii')
const PC_HEADER_BASE = Buffer.from('::PCG::', 'ascii')
const POST_ID_SEPARATOR = Buffer.from('::', 'ascii')

const IOT_HEADER_END = IOT_HEADER_LEN + ID_LEN + 2

const PING = Buffer.from('FF', 'hex')
const PING_RESPONSE = Buffer.from('EE', 'hex')
const DISCOVER = Buffer.from('BB', 'hex')
const DISCOVER_RESPONSE = Buffer.from('AA', 'hex')
const SPECIAL_FUNCTION = Buffer.from('AF', 'hex')

const SUCCESS_CODE = Buffer.from('CC', 'hex')
const FAILURE_CODE = Buffer.from('DD', 'hex')

const COMMAND_CODE_POS = 0
const PERIPHERAL_CODE_POS = 1
const PIN_NUM_POS = 2
const PERIPHERAL_MODE_POS = 3
const DATA_POS = 4

// IOT Packet: ::IOT::<UUID of 12 bytes>::<msg>
/* PC Packet: ::PCGUI::<MAC ID of 12 characters>::<msg>
*  Ping: <msg> = #PING#
*/

function sendMessage (info, cb) {
  if (!(info.server && info.msg && info.mac && info.address && info.port)) {
    return
  }
  info.mac = Buffer.from(info.mac, 'hex')
  const msgBuff = Buffer.concat([PC_HEADER_BASE, info.mac, POST_ID_SEPARATOR, info.msg], PC_HEADER_BASE.length + info.mac.length + POST_ID_SEPARATOR.length + info.msg.length)
  // console.log(`Sending ${msgBuff}`)
  Server.sendMessage(info.server, msgBuff, info.address, info.port, cb)
}

function sendDiscovery (info) {
  info.msg = DISCOVER
  info.address = Server.BROADCAST_IP
  info.server.setBroadcast(true)
  sendMessage(info, (err) => {
    if (err) {
      console.log('Discover transmit error', err)
    }
  })
}

function sendSpecialFunctionData (info, data) {
  const msgBuff = Buffer.concat([SPECIAL_FUNCTION, data], SPECIAL_FUNCTION.length + data.length)
  info.msg = msgBuff
  sendMessage(info, (err) => {
    if (err) {
      console.log('SF Data transmit error', err)
    }
  })
}

export default {
  IOT_HEADER,
  PC_HEADER,
  PING,
  PING_RESPONSE,
  DISCOVER,
  DISCOVER_RESPONSE,
  DATA_POS,
  PERIPHERAL_MODE_POS,
  PIN_NUM_POS,
  PERIPHERAL_CODE_POS,
  COMMAND_CODE_POS,
  SUCCESS_CODE,
  FAILURE_CODE,
  verifyHeader (msg) {
    if (msg.length < IOT_HEADER.length) {
      console.log(`Received Header too short: ${msg.length}`)
      return false
    }
    var res = ((msg.compare(IOT_HEADER, HEADER_START, IOT_HEADER_LEN, HEADER_START, IOT_HEADER_LEN) === 0) &&
        (msg.compare(IOT_HEADER, IOT_HEADER_LEN + ID_LEN, IOT_HEADER_END, IOT_HEADER_LEN + ID_LEN, IOT_HEADER_END) === 0))
    return res
  },
  sendMessage,
  sendDiscovery,
  sendSpecialFunctionData,
  getID (msg) {
    const buff = Buffer.allocUnsafe(ID_LEN)
    msg.copy(buff, 0, IOT_HEADER_LEN, IOT_HEADER_LEN + ID_LEN)
    return buff
  },
  getCommandCode (msg) {
    return msg[COMMAND_CODE_POS]
  },
  getPeripheralCode (msg) {
    return msg[PERIPHERAL_CODE_POS]
  },
  getFieldPinNum (msg) {
    return msg[PIN_NUM_POS]
  },
  getPeripheralMode (msg) {
    return msg[PERIPHERAL_MODE_POS]
  },
  getData (msg) {
    const buff = Buffer.allocUnsafe(msg.length - DATA_POS)
    msg.copy(buff, 0, DATA_POS, msg.length)
    return buff
  },
  getPacketContents (msg) {
    const buff = Buffer.allocUnsafe(msg.length - IOT_HEADER.length)
    msg.copy(buff, 0, IOT_HEADER.length, msg.length)
    return buff
  },
  setCommandCode (msg, code) {
    msg[COMMAND_CODE_POS] = code
  },
  setPeripheralCode (msg, code) {
    msg[PERIPHERAL_CODE_POS] = code
  },
  setFieldPinNum (msg, num) {
    msg[PIN_NUM_POS] = num
  },
  setPeripheralMode (msg, mode) {
    msg[PERIPHERAL_MODE_POS] = mode
  },
  setData (msg, data) {
    const msgBuff = Buffer.concat([msg, data], msg.length + data.length)
    return msgBuff
  },
  isCorrespondingAck (msg, ack) {
    return (msg.compare(ack, 0, DATA_POS - 1, 0, DATA_POS - 1) === 0)
  },
  isPingResponse (msg) { // Is this packet in response to a ping?
    const res = msg.compare(PING_RESPONSE, 0, PING_RESPONSE.length, IOT_HEADER_END, IOT_HEADER_END + PING_RESPONSE.length)
    return (res === 0)
  },
  isDiscoverResponse (msg) { // Is this packet in response to a discovery packet?
    const res = msg.compare(DISCOVER_RESPONSE, 0, DISCOVER_RESPONSE.length, IOT_HEADER_END, IOT_HEADER_END + DISCOVER_RESPONSE.length)
    return (res === 0)
  },
  isSpecialFunctionPacket (msg) {
    const res = msg.compare(SPECIAL_FUNCTION, 0, SPECIAL_FUNCTION.length, IOT_HEADER_END, IOT_HEADER_END + SPECIAL_FUNCTION.length)
    return (res === 0)
  },
  isSuccessCode (byte) {
    return (byte === SUCCESS_CODE[0])
  },
  isErrorCode (byte) {
    return (byte > SUCCESS_CODE[0] && byte <= FAILURE_CODE[0])
  }
}
