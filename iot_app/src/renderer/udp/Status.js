const ConnectionStatus = Object.freeze({
  'ACTIVE': 1,
  'TIMEOUT': 2,
  'IDLE': 3
})

export default {
  ConnectionStatus
}
