import os from 'os'
// import getmac from 'getmac'

export default {
  getNetworkInterfaces () {
    return os.networkInterfaces()
  },
  getMacAndIPv4 (netif) {
    for (var i in netif) {
      console.log(i)
      if (netif[i].family === 'IPv4') {
        return { mac: netif[i].mac, ip: netif[i].address }
      }
    }
    return null
  },
  macStripSemi (mac) {
    var arr = mac.split(':')
    var newMac = ''
    for (var i in arr) {
      newMac += arr[i]
    }
    return newMac
  }
}
