const PeripheralCodes = Object.freeze({
  'UNASSIGNED': 0,
  'GPIO': 1,
  'ANALOG': 2,
  'PWM': 3,
  'UART': 4
})

const PeripheralStates = Object.freeze({
  'OFF': 6,
  'CONFIGURE': 1,
  'IDLE': 5,
  'READ': 2,
  'WRITE': 3,
  'ASSIGN': 4
})

const PeripheralModes = Object.freeze({
  'UNASSIGNED': 0,
  'OUTPUT': 1,
  'INPUT': 2
})

const AnalogResolutions = Object.freeze({
  12: 0,
  10: 1,
  8: 2,
  6: 3
})

function getPeripheralName (code) {
  switch (code) {
    case PeripheralCodes.ANALOG:
      return 'Analog'
    case PeripheralCodes.GPIO:
      return 'GPIO'
    case PeripheralCodes.PWM:
      return 'PWM'
    case PeripheralCodes.UART:
      return 'UART'
    default:
      return 'Null'
  }
}

function getPeripheralStateName (state) {
  switch (state) {
    case PeripheralStates.OFF:
      return 'Off'
    case PeripheralStates.CONFIGURE:
      return 'Configuration'
    case PeripheralStates.IDLE:
      return 'Idle'
    case PeripheralStates.READ:
      return 'Read'
    case PeripheralStates.WRITE:
      return 'Write'
    default:
      return 'Null'
  }
}

function getPeripheralModeName (mode) {
  switch (mode) {
    case PeripheralModes.OUTPUT:
      return 'Output/TX'
    case PeripheralModes.INPUT:
      return 'Input/RX'
    default:
      return 'Off'
  }
}

class Peripheral {
  constructor (type) {
    this.data = {
      type,
      name: getPeripheralName(type),
      possiblePins: [],
      currentPins: []
    }
  }

  addPossiblePin (pin) {
    this.data.possiblePins.push(pin)
  }

  getPossiblePins () {
    return this.data.possiblePins
  }

  addPin (pin) {
    const pinType = pin.types[this.data.type]
    if (pinType) {
      pin.currentType = this.data.type
      pin.currentState = PeripheralStates.OFF
      pin.pendingAcks = []
      pin.extraData = {}
      switch (pin.currentType) {
        case PeripheralCodes.ANALOG:
          pin.extraData.maxAnalogVoltage = -1
          pin.extraData.currentAnalogValue = -1
          break
          // TODO other pin types
        default:
          break
      }
      pin.extraData.currentPinOutput = 0
      this.data.currentPins.push(pin)
    } else {
      console.log('Invalid pin added to peripheral.')
    }
  }

  getPin (pinNum) {
    for (var i = 0; i < this.possiblePins.length; i++) {
      if (this.possiblePins[i].num === pinNum) {
        return this.possiblePins[i]
      }
    }
  }

  getPinByName (name) {
    for (var i = 0; i < this.possiblePins.length; i++) {
      if (this.possiblePins[i].name === name) {
        return this.possiblePins[i]
      }
    }
  }

  getCurrentPins () {
    return this.data.currentPins
  }

  removePin (pin) {
    pin.currentType = PeripheralCodes.UNASSIGNED
    pin.currentMode = PeripheralModes.UNASSIGNED
    pin.currentState = PeripheralStates.OFF
    pin.pendingAcks = []
    pin.extraData = {}
    const idx = this.data.currentPins.indexOf(pin)
    if (idx !== -1) this.data.currentPins.splice(idx, 1)
  }

  getType () {
    return this.data.type
  }

  getName () {
    return this.data.name
  }
}

export default {
  PeripheralCodes,
  PeripheralStates,
  PeripheralModes,
  Peripheral,
  getPeripheralName,
  getPeripheralStateName,
  getPeripheralModeName,
  AnalogResolutions
}
