import dgram from 'dgram'

const BROADCAST_IP = '255.255.255.255'

export default {
  BROADCAST_IP,
  bindSocket (localIP, localPort, cb) {
    const server = dgram.createSocket('udp4')
    console.log('Binding UDP socket')
    server.on('listening', () => {
      const address = server.address()
      console.log(`server listening ${address.address}:${address.port}`)
      cb(server, address.address, address.port)
    })
    server.bind({
      address: localIP,
      port: localPort,
      exclusive: true
    })
  },
  setOnError (server, cb) {
    server.on('error', (err) => {
      console.log(`server error:\n${err.stack}. Closing socket.`)
      server.close()
      cb(err)
    })
  },
  setOnClose (server, cb) {
    server.on('close', () => {
      console.log('Closing server port.')
      cb()
    })
  },
  setOnMessage (server, cb) {
    server.on('message', (msg, rinfo) => {
      console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`)
      cb(msg, rinfo)
    })
  },
  sendMessage (server, msg, address, port, cb) {
    server.send(msg, port, address, (err) => {
      if (err) {
        cb(err)
      } else {
        cb()
      }
    })
  }
}
