import Peripherals from './Peripherals'

const PINS = Object.freeze({
  33: {
    num: 33,
    name: 'UIO_0',
    types: {
      [Peripherals.PeripheralCodes.GPIO]: {
        'modes': [
          Peripherals.PeripheralModes.INPUT,
          Peripherals.PeripheralModes.OUTPUT
        ]
      },
      [Peripherals.PeripheralCodes.ANALOG]: {
        'modes': [
          Peripherals.PeripheralModes.INPUT,
          Peripherals.PeripheralModes.OUTPUT
        ]
      }
    },
    currentType: Peripherals.PeripheralCodes.UNASSIGNED,
    currentMode: Peripherals.PeripheralModes.UNASSIGNED,
    currentState: Peripherals.PeripheralStates.OFF,
    pendingAcks: [],
    extraData: {}
  },
  8: {
    num: 8,
    name: 'UIO_1',
    types: {
      [Peripherals.PeripheralCodes.GPIO]: {
        'modes': [
          Peripherals.PeripheralModes.INPUT,
          Peripherals.PeripheralModes.OUTPUT
        ]
      },
      [Peripherals.PeripheralCodes.ANALOG]: {
        'modes': [
          Peripherals.PeripheralModes.INPUT,
          Peripherals.PeripheralModes.OUTPUT
        ]
      }
    },
    currentType: Peripherals.PeripheralCodes.UNASSIGNED,
    currentMode: Peripherals.PeripheralModes.UNASSIGNED,
    currentState: Peripherals.PeripheralStates.OFF,
    pendingAcks: [],
    extraData: {}
  },
  20: {
    num: 20,
    name: 'UIO_2',
    types: {
      [Peripherals.PeripheralCodes.GPIO]: {
        'modes': [
          Peripherals.PeripheralModes.INPUT,
          Peripherals.PeripheralModes.OUTPUT
        ]
      },
      [Peripherals.PeripheralCodes.ANALOG]: {
        'modes': [
          Peripherals.PeripheralModes.INPUT,
          Peripherals.PeripheralModes.OUTPUT
        ]
      }
    },
    currentType: Peripherals.PeripheralCodes.UNASSIGNED,
    currentMode: Peripherals.PeripheralModes.UNASSIGNED,
    currentState: Peripherals.PeripheralStates.OFF,
    pendingAcks: [],
    extraData: {}
  },
  21: {
    num: 21,
    name: 'PWM_0',
    types: {
      [Peripherals.PeripheralCodes.GPIO]: {
        'modes': [
          Peripherals.PeripheralModes.INPUT,
          Peripherals.PeripheralModes.OUTPUT
        ]
      },
      [Peripherals.PeripheralCodes.PWM]: {
        'modes': [
          Peripherals.PeripheralModes.INPUT,
          Peripherals.PeripheralModes.OUTPUT
        ]
      }
    },
    currentType: Peripherals.PeripheralCodes.UNASSIGNED,
    currentMode: Peripherals.PeripheralModes.UNASSIGNED,
    currentState: Peripherals.PeripheralStates.OFF,
    pendingAcks: [],
    extraData: {}
  }
})

export default {
  PINS,
  MAX_TIMEOUT: 10,
  NUM_TIMEOUTS: 2,
  getPinsObject () {
    return JSON.parse(JSON.stringify(PINS))
  }
}
