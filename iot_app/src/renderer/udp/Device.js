import Protocol from './Protocol.js'
import Status from './Status.js'
import Pins from './Pins.js'
import Peripherals from './Peripherals.js'

export default class Device {
  constructor () {
    this.info = {
      displayID: '',
      id: Buffer.allocUnsafe(12),
      ip: '',
      port: 0,
      timeout: 0,
      timeoutReload: 0,
      destroyFlag: false,
      state: null, // TODO write states
      connectionState: Status.ConnectionStatus.IDLE
    }
    this.data = {
      pinsList: Pins.getPinsObject(),
      peripheralList: []
    }

    this.gui = {
      selectedPeripheral: null,
      selectedPin: {},
      currentPinTab: {}
    }

    this.timer = null

    for (var p in Peripherals.PeripheralCodes) {
      const peripheralCode = Peripherals.PeripheralCodes[p]
      if (peripheralCode !== Peripherals.PeripheralCodes.UNASSIGNED) {
        const periph = new Peripherals.Peripheral(peripheralCode)
        for (var pin in this.data.pinsList) {
          pin = this.data.pinsList[pin]
          if (pin.types[peripheralCode]) {
            periph.addPossiblePin(pin)
            this.gui.currentPinTab[periph.getType()] = pin.name
          }
        }
        this.data.peripheralList.push(periph)
      }
    }
  }

  getPeripherals () {
    return this.data.peripheralList
  }

  setIP (ip) {
    this.info.ip = ip
    return this
  }

  setPort (port) {
    this.info.port = port
    return this
  }

  setID (ID) {
    this.info.id = ID
    this.info.displayID = ID.toString('hex')
    return this
  }

  setTimeout (timeout) {
    this.info.timeout = timeout
    this.info.timeoutReload = timeout
    return this
  }

  decTimer () {
    this.info.timeout--
  }

  renewTimeout () {
    this.info.connectionState = Status.ConnectionStatus.IDLE
    this.info.timeout = this.info.timeoutReload
    return this
  }

  ping (info) {
    info.msg = Protocol.PING
    Protocol.sendMessage(info, (err) => {
      if (err) {
        console.log('Ping error')
      }
    })
  }

  addPendingAck (pin, msg) {
    pin.pendingAcks.push({ msg, timeout: Pins.MAX_TIMEOUT, numTimeouts: 0 })
  }

  removeDuplicateAcks (pin, msg) {
    let i = pin.pendingAcks.length
    while (i--) {
      if (Protocol.isCorrespondingAck(msg, pin.pendingAcks[i].msg)) {
        pin.pendingAcks.splice(i, 1)
      }
    }
  }

  configurePin (info, peripheral, pin, mode) {
    var msg = null
    if (peripheral.getType() === Peripherals.PeripheralCodes.GPIO) {
      msg = Buffer.allocUnsafe(Protocol.DATA_POS)
      msg[Protocol.COMMAND_CODE_POS] = Peripherals.PeripheralStates.CONFIGURE
      msg[Protocol.PERIPHERAL_CODE_POS] = Peripherals.PeripheralCodes.GPIO
      msg[Protocol.PIN_NUM_POS] = pin.num
      msg[Protocol.PERIPHERAL_MODE_POS] = mode
    } else if (peripheral.getType() === Peripherals.PeripheralCodes.ANALOG) {
      msg = Buffer.allocUnsafe(Protocol.DATA_POS + 2)
      msg[Protocol.COMMAND_CODE_POS] = Peripherals.PeripheralStates.CONFIGURE
      msg[Protocol.PERIPHERAL_CODE_POS] = Peripherals.PeripheralCodes.ANALOG
      msg[Protocol.PIN_NUM_POS] = pin.num
      msg[Protocol.PERIPHERAL_MODE_POS] = mode
      msg[Protocol.DATA_POS] = Buffer.from('R', 'ascii')[0]
      msg[Protocol.DATA_POS + 1] = pin.extraData.resolution // Remember to set resolution for analog setup
    } else if (peripheral.getType() === Peripherals.PeripheralCodes.PWM) {
      // TODO
    } else {
      return
    }
    // TODO more
    this.removeDuplicateAcks(pin, msg)

    info.msg = msg
    Protocol.sendMessage(info, (err) => {
      if (err) {
        console.log('Configure transmission error')
      }
      console.log('Pushing command onto pending ACKs')
      this.addPendingAck(pin, msg)
    })
  }

  pinOutput (info, peripheral, pin, data) {
    let msg = null
    console.log(`Pin output: ${data}`)
    switch (peripheral.getType()) {
      case Peripherals.PeripheralCodes.GPIO:
        msg = Buffer.allocUnsafe(Protocol.DATA_POS + 2)
        msg[Protocol.COMMAND_CODE_POS] = Peripherals.PeripheralStates.WRITE
        msg[Protocol.PERIPHERAL_CODE_POS] = Peripherals.PeripheralCodes.GPIO
        msg[Protocol.PIN_NUM_POS] = pin.num
        msg[Protocol.PERIPHERAL_MODE_POS] = Peripherals.PeripheralModes.OUTPUT
        msg[Protocol.DATA_POS] = Buffer.from('V', 'ascii')[0]
        msg[Protocol.DATA_POS + 1] = data
        break
      case Peripherals.PeripheralCodes.PWM:
        break
      case Peripherals.PeripheralCodes.ANALOG:
        break
      case Peripherals.PeripheralCodes.UART:
        break
      default:
        return
    }
    this.removeDuplicateAcks(pin, msg)

    info.msg = msg
    Protocol.sendMessage(info, (err) => {
      if (err) {
        console.log('Configure transmission error')
      }
      console.log('Pushing command onto pending ACKs')
      this.addPendingAck(pin, msg)
    })
  }

  // eslint-disable-next-line
  pinInput (info, peripheral, pin, data) {
    let msg = null
    switch (peripheral.getType()) {
      case Peripherals.PeripheralCodes.GPIO:
        break
      case Peripherals.PeripheralCodes.PWM:
        break
      case Peripherals.PeripheralCodes.ANALOG:
        // pin.extraData.currentAnalogValue = -1
        msg = Buffer.allocUnsafe(Protocol.DATA_POS)
        msg[Protocol.COMMAND_CODE_POS] = Peripherals.PeripheralStates.READ
        msg[Protocol.PERIPHERAL_CODE_POS] = Peripherals.PeripheralCodes.ANALOG
        msg[Protocol.PIN_NUM_POS] = pin.num
        msg[Protocol.PERIPHERAL_MODE_POS] = Peripherals.PeripheralModes.INPUT
        console.log(`Pin input: ${msg}`)
        break
      case Peripherals.PeripheralCodes.UART:
        break
      default:
        break
    }

    info.msg = msg
    Protocol.sendMessage(info, (err) => {
      if (err) {
        console.log('Configure transmission error')
      }
      console.log('Pushing command onto pending ACKs')
      this.addPendingAck(pin, msg)
    })
  }

  getIP () {
    return this.info.ip
  }

  getPort () {
    return this.info.port
  }

  getID () {
    return this.info.id
  }

  getDisplayID () {
    return this.info.displayID
  }

  getConnectionState () {
    return this.info.connectionState
  }

  compareID (otherID) {
    return (this.info.id.compare(otherID, 0, Protocol.ID_LEN, 0, Protocol.ID_LEN) === 0)
  }

  getTimedOutStatus () {
    return (this.info.connectionState === Status.ConnectionStatus.TIMEOUT)
  }

  setTimedOut () {
    this.info.connectionState = Status.ConnectionStatus.TIMEOUT
  }

  getTimeOut () {
    return this.info.timeout
  }

  destroy () {
    this.info.destroyFlag = true
  }

  destroyed () {
    return this.info.destroyFlag
  }

  selectPeripheral (peripheral) {
    this.gui.selectedPeripheral = peripheral
  }

  selectPin (pin, peripheral) {
    this.gui.selectedPin[peripheral] = pin
  }

  selectTab (tab, peripheral) {
    this.gui.currentPinTab[peripheral] = tab
  }

  getSelectedPeripheral () {
    return this.gui.selectedPeripheral
  }

  getSelectedPin (peripheral) {
    return this.gui.selectedPin[peripheral]
  }

  getSelectedTab (peripheral) {
    return this.gui.currentPinTab[peripheral]
  }

  getAllPinsList () {
    return this.data.pinsList
  }
}
